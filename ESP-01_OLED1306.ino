
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "eflogo.h"
#include <ArduinoJson.h>
#include <Fonts/FreeSerifBold18pt7b.h>
#include <Fonts/FreeSansBold12pt7b.h>

#define OLED_RESET 4 // LED_BUILTIN  //4
Adafruit_SSD1306 display(OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

// =======================================================================
// Конфигурация устройства:
// =======================================================================
const char* ssid     = "FreeCAD";                      // SSID
const char* password = "Free@CAD";                    // пароль
String weatherKey = "96e8d04f517bf50b586082c213eb24c2";  // Чтобы получить API ключь, перейдите по ссылке http://openweathermap.org/api
String weatherLang = "&lang=ru";
String cityID = "703448"; //Kiev
// =======================================================================

WiFiClient client;
// ======================== Погодные переменные
String weatherMain = "";
String weatherDescription = "";
String weatherLocation = "";
String country;
int humidity;
int pressure;
float temp;
float tempMin, tempMax;
int clouds;
float windSpeed;
String date;
String currencyRates;
String weatherString;


void setup()   {                


//    Serial.begin(115200);                           // Дебаг
    
// ======================== Соединение с WIFI
  WiFi.mode(WIFI_STA); 
  WiFi.begin(ssid, password);                         // Подключаемся к WIFI
  while (WiFi.status() != WL_CONNECTED) {         // Ждем до посинения
    delay(500);
//    Serial.print(".");
  }

  
  Wire.pins(0, 2); //для ESP-01.

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // тип дисплея - ssd1306 128x64
  display.clearDisplay();

  display.drawBitmap(0, 0,  eflogo, 127, 64, WHITE);  // выводим изображение (X, Y, bmp, ширина, высота, цвет)
  display.display();
  delay(3000);     
  display.startscrolldiagright(0x00, 0x07);
  delay(2000);
  display.startscrolldiagleft(0x00, 0x07);
  delay(2000);
  display.stopscroll();
  delay(4000);
  display.clearDisplay();



}

// =========================== Переменные времени
#define MAX_DIGITS 16
byte dig[MAX_DIGITS]={0};
byte digold[MAX_DIGITS]={0};
byte digtrans[MAX_DIGITS]={0};
int updCnt = 0;
int dots = 0;
long dotTime = 0;
long clkTime = 0;
int dx=0;
int dy=0;
byte del=0;
int h,m,s;
// =======================================================================
void loop() {

if(updCnt<=0) { // каждые 10 циклов получаем данные времени и погоды
    updCnt = 10;
//    Serial.println("Getting data ...");
    getWeatherData();
    getTime();
//    Serial.println("Data loaded");
    clkTime = millis();
  }

  if(millis()-clkTime > 10000 && !del && dots) { //каждые 15 секунд 
    
    pogoda();
    updCnt--;
    clkTime = millis();
  }

  
  vremya();
  if(millis()-dotTime > 500) {
    dotTime = millis();
    dots = !dots;
  }
}
//========================================================================
void pogoda(void) {
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(utf8rus("    ПОГОДА В КИЕВЕ"));
  display.drawBitmap(15, 20, eft, 24, 40, WHITE);  // выводим изображение (X, Y, bmp, ширина, высота, цвет)
  display.setFont(&FreeSerifBold18pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(45,45);
  display.println(String(temp,0));
  display.setFont(&FreeSansBold12pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(95,45);
  display.println(" C");
  display.display();
  display.setFont();
  delay(3000);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(utf8rus("    ПОГОДА В КИЕВЕ"));
  display.drawBitmap(8, 20, efh, 32, 40, WHITE);  // выводим изображение (X, Y, bmp, ширина, высота, цвет)
  display.setFont(&FreeSerifBold18pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(55,45);
  display.println(String(humidity));
  display.setFont(&FreeSansBold12pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(95,45);
  display.println("%");
  display.display();
  display.setFont();
  delay(3000);
}
//========================================================================
void vremya(void) {
  updateTime();
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println(utf8rus("   КИЕВСКОЕ  ВРЕМЯ"));
  display.setFont(&FreeSerifBold18pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(5,45);
  display.println(String(h/10)+String(h%10)+":"+String(m/10)+String(m%10));
  display.setFont(&FreeSansBold12pt7b);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(95,45);
  display.println(String(s/10)+String(s%10));
  display.display();
  display.setFont();
}
//========================================================================
void testscrolltext(void) {
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(10,0);
  display.clearDisplay();
  display.println("scroll");
  display.display();
  delay(1);
 
  display.startscrollright(0x00, 0x0F);
  delay(2000);
  display.stopscroll();
  delay(1000);
  display.startscrollleft(0x00, 0x0F);
  delay(2000);
  display.stopscroll();
  delay(1000);    
  display.startscrolldiagright(0x00, 0x07);
  delay(2000);
  display.startscrolldiagleft(0x00, 0x07);
  delay(2000);
  display.stopscroll();
}

// =======================================================================
// Берем погоду с сайта openweathermap.org
// =======================================================================



const char *weatherHost = "api.openweathermap.org";

void getWeatherData()
{
//  Serial.print("connecting to "); Serial.println(weatherHost);
  if (client.connect(weatherHost, 80)) {
    client.println(String("GET /data/2.5/weather?id=") + cityID + "&units=metric&appid=" + weatherKey + weatherLang + "\r\n" +
                "Host: " + weatherHost + "\r\nUser-Agent: ArduinoWiFi/1.1\r\n" +
                "Connection: close\r\n\r\n");
  } else {
//    Serial.println("connection failed");
    return;
  }
  String line;
  int repeatCounter = 0;
  while (!client.available() && repeatCounter < 10) {
    delay(500);
    Serial.println("w.");
    repeatCounter++;
  }
  while (client.connected() && client.available()) {
    char c = client.read(); 
    if (c == '[' || c == ']') c = ' ';
    line += c;
  }

  client.stop();

  DynamicJsonBuffer jsonBuf;
  JsonObject &root = jsonBuf.parseObject(line);
  if (!root.success())
  {
//    Serial.println("parseObject() failed");
    return;
  }
  //weatherMain = root["weather"]["main"].as<String>();
  weatherDescription = root["weather"]["description"].as<String>();
  weatherDescription.toLowerCase();
  //  weatherLocation = root["name"].as<String>();
  //  country = root["sys"]["country"].as<String>();
  temp = root["main"]["temp"];
  humidity = root["main"]["humidity"];
  pressure = root["main"]["pressure"];
  tempMin = root["main"]["temp_min"];
  tempMax = root["main"]["temp_max"];
  windSpeed = root["wind"]["speed"];
  clouds = root["clouds"]["all"];
  String deg = String(char('~'+25));
  weatherString = "         Temp: " + String(temp,1) + deg + "C (" + String(tempMin,1) + deg + "-" + String(tempMax,1) + deg + ")  ";
  weatherString += weatherDescription;
  weatherString += "  Humidity: " + String(humidity) + "%  ";
  weatherString += "  Pressure: " + String(pressure) + "hPa  ";
  weatherString += "  Clouds: " + String(clouds) + "%  ";
  weatherString += "  Wind: " + String(windSpeed,1) + "m/s                 ";
}

// =======================================================================
// Берем время у GOOGLE
// =======================================================================

float utcOffset = 3; //поправка часового пояса
long localEpoc = 0;
long localMillisAtUpdate = 0;

void getTime()
{
  WiFiClient client;
  if (!client.connect("www.google.com", 80)) {
//    Serial.println("connection to google failed");
    return;
  }

  client.print(String("GET / HTTP/1.1\r\n") +
               String("Host: www.google.com\r\n") +
               String("Connection: close\r\n\r\n"));
  int repeatCounter = 0;
  while (!client.available() && repeatCounter < 10) {
    delay(500);
    //Serial.println(".");
    repeatCounter++;
  }

  String line;
  client.setNoDelay(false);
  while(client.connected() && client.available()) {
    line = client.readStringUntil('\n');
    line.toUpperCase();
    if (line.startsWith("DATE: ")) {
      date = "     "+line.substring(6, 22);
      h = line.substring(23, 25).toInt();
      m = line.substring(26, 28).toInt();
      s = line.substring(29, 31).toInt();
      localMillisAtUpdate = millis();
      localEpoc = (h * 60 * 60 + m * 60 + s);
    }
  }
  client.stop();
}

// =======================================================================r

void updateTime()
{
  long curEpoch = localEpoc + ((millis() - localMillisAtUpdate) / 1000);
  long epoch = round(curEpoch + 3600 * utcOffset + 86400L) % 86400L;
  h = ((epoch  % 86400L) / 3600) % 24;
  m = (epoch % 3600) / 60;
  s = epoch % 60;
}

// ================================ Вывод Русских Букв
String utf8rus(String source)
{
  int i,k;
  String target;
  unsigned char n;
  char m[2] = { '0', '\0' };

  k = source.length(); i = 0;

  while (i < k) {
    n = source[i]; i++;

    if (n >= 0xC0) {
      switch (n) {
        case 0xD0: {
          n = source[i]; i++;
          if (n == 0x81) { n = 0xA8; break; }
          if (n >= 0x90 && n <= 0xBF) n = n + 0x30-1;
          break;
        }
        case 0xD1: {
          n = source[i]; i++;
          if (n == 0x91) { n = 0xB8; break; }
          if (n >= 0x80 && n <= 0x8F) n = n + 0x70-1;
          break;
        }
      }
    }
    m[0] = n; target = target + String(m);
  }
return target;
}
